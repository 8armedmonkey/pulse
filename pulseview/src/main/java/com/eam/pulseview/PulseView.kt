package com.eam.pulseview

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.core.content.res.use
import kotlin.math.min

class PulseView : View {

    private val viewCenterX
        get() = width / 2

    private val viewCenterY
        get() = height / 2

    private val viewShortestEdge
        get() = min(width, height)

    private var dotRadiusRatio = DEFAULT_DOT_RADIUS_RATIO
    private var dotColor = DEFAULT_DOT_COLOR
    private var pulseMinRadiusRatio = DEFAULT_PULSE_MIN_RADIUS_RATIO
    private var pulseMaxRadiusRatio = DEFAULT_PULSE_MAX_RADIUS_RATIO
    private var pulseMinAlpha = DEFAULT_PULSE_MIN_ALPHA
    private var pulseMaxAlpha = DEFAULT_PULSE_MAX_ALPHA
    private var pulseColor = dotColor
    private var animationDurationMillis = DEFAULT_ANIMATION_DURATION_MILLIS

    private val pulseAnimator = ValueAnimator.ofFloat(0.0f, 1.0f).apply {
        repeatMode = ValueAnimator.RESTART
        repeatCount = ValueAnimator.INFINITE
        duration = animationDurationMillis
        interpolator = DecelerateInterpolator(2.0f)
        addUpdateListener { invalidate() }
    }

    private val paint = Paint().apply {
        isAntiAlias = true
        isDither = true
        style = Paint.Style.FILL
    }

    constructor(context: Context?) : super(context)

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.let {
            drawPulse(it)
            drawDot(it)
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        startPulsating()
    }

    override fun onDetachedFromWindow() {
        stopPulsating()
        super.onDetachedFromWindow()
    }

    private fun init(attrs: AttributeSet?) {
        context.obtainStyledAttributes(attrs, R.styleable.PulseView).use {
            dotRadiusRatio = it.getFloat(
                R.styleable.PulseView_dotRadiusRatio,
                DEFAULT_DOT_RADIUS_RATIO
            )
            dotColor = it.getColor(
                R.styleable.PulseView_dotColor,
                DEFAULT_DOT_COLOR
            )
            pulseMinRadiusRatio = it.getFloat(
                R.styleable.PulseView_pulseMinRadiusRatio,
                DEFAULT_PULSE_MIN_RADIUS_RATIO
            )
            pulseMaxRadiusRatio = it.getFloat(
                R.styleable.PulseView_pulseMaxRadiusRatio,
                DEFAULT_PULSE_MAX_RADIUS_RATIO
            )
            pulseMinAlpha = it.getFloat(
                R.styleable.PulseView_pulseMinAlpha,
                DEFAULT_PULSE_MIN_ALPHA
            )
            pulseMaxAlpha = it.getFloat(
                R.styleable.PulseView_pulseMaxAlpha,
                DEFAULT_PULSE_MAX_ALPHA
            )
            pulseColor = it.getColor(
                R.styleable.PulseView_pulseColor,
                dotColor
            )
            animationDurationMillis = it.getInteger(
                R.styleable.PulseView_animationDurationMillis,
                DEFAULT_ANIMATION_DURATION_MILLIS.toInt()
            ).toLong()
        }
    }

    private fun startPulsating() {
        if (pulseAnimator.isStarted.not()) {
            pulseAnimator.start()
        }
    }

    private fun stopPulsating() {
        pulseAnimator.cancel()
    }

    private fun drawDot(canvas: Canvas) {
        val dotRadiusPixels = dotRadiusRatio * viewShortestEdge

        canvas.apply {
            save()
            translate(viewCenterX.toFloat(), viewCenterY.toFloat())
            drawOval(
                -dotRadiusPixels,
                -dotRadiusPixels,
                dotRadiusPixels,
                dotRadiusPixels,
                paint.also { it.color = dotColor })
            restore()
        }
    }

    private fun drawPulse(canvas: Canvas) {
        val pulseAnimatedGrowthRatio =
            (pulseMaxRadiusRatio - pulseMinRadiusRatio) * pulseAnimator.animatedValue as Float
        val pulseAnimatedRadiusRatio = pulseMinRadiusRatio + pulseAnimatedGrowthRatio
        val pulseRadiusPixels = pulseAnimatedRadiusRatio * viewShortestEdge
        val pulseAnimatedShrinkAlpha =
            (pulseMaxAlpha - pulseMinAlpha) * pulseAnimator.animatedValue as Float
        val pulseAlpha = pulseMaxAlpha - pulseAnimatedShrinkAlpha

        canvas.apply {
            save()
            translate(viewCenterX.toFloat(), viewCenterY.toFloat())
            drawOval(
                -pulseRadiusPixels,
                -pulseRadiusPixels,
                pulseRadiusPixels,
                pulseRadiusPixels,
                paint.also { p ->
                    p.color = pulseColor.let { c ->
                        Color.argb(
                            (pulseAlpha * 255).toInt(),
                            Color.red(c),
                            Color.green(c),
                            Color.blue(c)
                        )
                    }
                }
            )
            restore()
        }
    }

    companion object {

        private const val DEFAULT_DOT_RADIUS_RATIO = 0.10f
        private const val DEFAULT_DOT_COLOR = Color.BLACK
        private const val DEFAULT_PULSE_MIN_RADIUS_RATIO = 0.15f
        private const val DEFAULT_PULSE_MAX_RADIUS_RATIO = 0.50f
        private const val DEFAULT_PULSE_MIN_ALPHA = 0.0f
        private const val DEFAULT_PULSE_MAX_ALPHA = 0.2f
        private const val DEFAULT_ANIMATION_DURATION_MILLIS = 2000L

    }

}